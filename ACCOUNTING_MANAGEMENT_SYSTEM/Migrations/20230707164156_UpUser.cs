﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ACCOUNTING_MANAGEMENT_SYSTEM.Migrations
{
    public partial class UpUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BranchId",
                table: "AspNetUsers",
                newName: "ComapanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ComapanyId",
                table: "AspNetUsers",
                newName: "BranchId");
        }
    }
}
