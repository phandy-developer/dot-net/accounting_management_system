﻿using System.ComponentModel.DataAnnotations;

namespace ACCOUNTING_MANAGEMENT_SYSTEM.Models
{
    public class AccountType
    {
        [Key]
        public int id { get; set; }
        public string? name { get; set; }
    }
}
